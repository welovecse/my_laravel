<head>
      <title> Brochill</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">      
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101761482-1', 'auto');
  ga('send', 'pageview');

</script>

    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
      

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


      <style type="text/css">
        
        input[type=text] {
    
    padding: 4px 4px;
    margin: 8px 0;
    box-sizing: border-box;
    height:20px;
    width: 60px;
    border: 1px solid gray; 
          }

          input[type=text] {
    
    padding: 4px 4px;
    margin: 8px 0;
    box-sizing: border-box;
    height:20px;
    width: 60px;
    border: 1px solid gray; 
          }
    h2{
        color: gray;

    }     
    
      </style>
  
   </head>
<div class="container" >
  <div class="card">
   
    <div class="card-header">
        <h4>
          Upload Images
        </h4>
    </div>

@csrf
    

      <div class="card-body card-padding">
      <form name="app_inputs_upload" method="POST" enctype="multipart/form-data"
            files=true action="/apps/images">

        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div>

        @if(isset($message))
               <small >{{$message['message']}} </small>
        @endif


      </div>
        <div class="row">
          <div class="col-sm-10">
              <div class="input-group">
                  <span class="input-group-addon">
                    <i class="md md-text-format"></i>
                    <small class="text-danger">*</small>
                  </span>
                  <div class="fg-line">
                      <input class="pull-left"  type="file"  name="photos[]" multiple>
                      <button class="btn bgm-teal btn-sm pull-right btn-info" type="submit">Upload Images</button>
                  </div>
                   </form>
                  
                     
      
                  <br> 
              </div>
          </div>
        </div>
        <div class="col-sm-1">
                      @if($message['filepath']!='')

                     <a href="http://localhost/my_laravel/public/Quotes.txt"  target="_blank" download><button class="btn bgm-teal btn-sm pull-right btn-warning">Download</button></a> 

                    @endif


                   

                  </div>
     
    </div>
  </div>
                        @if(($message['count']!=''))

                    
             <br><br>
             


  <div class="card">

      <div class="card-body card-padding">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
             <table class="table table-hover" align="center">
                <thead>
                      <tr>
                          <th>Image</th>
                          <th>Extracted text</th>
                          
                      </tr>
                </thead>
              <tbody>
                      @for($i=0;$i<$message['count'];$i++)
                        <tr>
                          <td><img src="{{$associativeArray[$i]['path']}}" width="250" height="250"> </td>
                          <td>{{$associativeArray[$i]['content']}}</td>
                          
                      </tr>
                      @endfor
                      
            </tbody>
          </table>
        </div>
         <div class="col-md-2"></div>
        




        </div>






       </div>
  </div>
  
                   @endif

</div>