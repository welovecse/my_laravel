<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     $message=array("message"=>"","filepath"=>"","count"=>0);
	 $associativeArray=array("path"=>"","content"=>"");

       return view('firstapp')->with(['message'=>$message,'associativeArray'=>$associativeArray]);
     //return view('welcome');
});


 
Route::post('/apps/images', 'UploadController@upload');
 
